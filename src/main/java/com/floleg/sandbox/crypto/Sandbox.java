package com.floleg.sandbox.crypto;

import java.security.NoSuchAlgorithmException;

public class Sandbox
{
    public static void main(String[] arg) throws NoSuchAlgorithmException {
        int expected = 746248918;
        int result = Sha256.getSha256AsInt31("5843");

        System.out.printf("Expected result: %s - Actual result: %s%n", expected, result);

        // andOperator();
    }

    private static void andOperator() {
        int val = 172 << 8;

        System.out.printf("val: %s%n", val);
    }
}
