package com.floleg.sandbox.crypto;

import java.security.*;
import java.security.spec.RSAPrivateKeySpec;

public class RsaKeyPair {
    public static void main(String[] arg) throws NoSuchAlgorithmException {
        // Generate RSA X9.31 keys
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(3072);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        System.out.println(privateKey.getFormat());
        System.out.println(publicKey.getFormat());
    }
}
