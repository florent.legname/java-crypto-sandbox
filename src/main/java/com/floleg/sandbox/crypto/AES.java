package com.floleg.sandbox.crypto;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class AES {
    public static void main(String[] arguments) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        String testValue = "e92e09a8ad63df41518fe74cd71a7439";
        String testKey = "264a0c14b2d68e550669db3b40a63450";
        // Expected result has been computed with former algorithm
        String expectedAESKey = "232dbb299b99ef839b1e1ed59b7e2c92";

        // Generate crypto.AES key
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(testKey.toCharArray(), testKey.getBytes(), 65536, 256);
        SecretKey secretKey = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");

        // Initialize cipher
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] cipherText = cipher.doFinal(testValue.getBytes());

        System.out.println(new String(cipherText, StandardCharsets.UTF_8));

        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decipheredText = cipher.doFinal();

        System.out.println(new String(decipheredText, StandardCharsets.UTF_8));
        System.out.println(expectedAESKey);
    }
}
