package com.floleg.sandbox.crypto;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class Sha256 {
    public static int getSha256AsInt31(String src) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] hash = messageDigest.digest(src.getBytes(StandardCharsets.UTF_8));

        // Select the first four bytes of the hash
        byte[] truncatedHash = Arrays.copyOfRange(hash, 0, 4);

        int val = 0;
        for (byte b : truncatedHash) {
            int x = b & 255;
            val = (val << 8) + x;
            System.out.println(val);
        }

        System.out.println(val);

        return val & 0x7fffffff;
    }
}
