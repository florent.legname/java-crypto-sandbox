package com.floleg.sandbox.crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.util.Arrays;
import java.util.logging.Logger;

public class RsaPss {
    public static void main(String[] arg)
            throws NoSuchPaddingException,
            IllegalBlockSizeException,
            NoSuchAlgorithmException,
            BadPaddingException,
            InvalidKeyException, SignatureException {
        // Generate RSA PSS keys
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSASSA-PSS");
        keyPairGenerator.initialize(3072);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        privateKey.getFormat();
        publicKey.getFormat();

        // Generate activation salt as 256 bits secure random
        SecureRandom random = SecureRandom.getInstanceStrong();
        byte[] activationSalt = new byte[32]; // 256 bit
        random.nextBytes(activationSalt);

        testCipherSignature(privateKey, publicKey, activationSalt);
        testSignatureClass(privateKey, publicKey, activationSalt);
    }

    public static void testCipherSignature(PrivateKey privateKey, PublicKey publicKey, byte[] activationSalt)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException,
            InvalidKeyException,
            IllegalBlockSizeException,
            BadPaddingException
    {
        // Encrypt the activation salt hash with RSA private key
        Cipher encryptionCipher = Cipher.getInstance("RSA");
        encryptionCipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] digitalSignature = encryptionCipher.doFinal(activationSalt);

        // Decrypt the encrypted activation salt hash with RSA public key
        Cipher decryptionCipher = Cipher.getInstance("RSA");
        decryptionCipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] decryptedMessage = decryptionCipher.doFinal(digitalSignature);

        boolean isCorrect = Arrays.equals(decryptedMessage, activationSalt);

        if (isCorrect) {
            Logger.getAnonymousLogger().info(String.format("Cipher signature isCorrect: %s", isCorrect));
        } else {
            Logger.getAnonymousLogger().info("Signature failure");
        }
    }

    public static void testSignatureClass(PrivateKey privateKey, PublicKey publicKey, byte[] activationSalt)
            throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signer = Signature.getInstance("NONEwithRSA");
        signer.initSign(privateKey);

        signer.update(activationSalt);
        byte[] digitalSignature = signer.sign();

        Signature signatureVerifier = Signature.getInstance("NONEwithRSA");
        signatureVerifier.initVerify(publicKey);
        signatureVerifier.update(activationSalt);
        boolean isCorrect = signatureVerifier.verify(digitalSignature);


        if (isCorrect) {
            Logger.getAnonymousLogger().info(String.format("Signature Class isCorrect: %s", isCorrect));
        } else {
            Logger.getAnonymousLogger().info("Signature Class failure");
        }
    }
}
